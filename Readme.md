# NPDE 15 build enviroment

Build environment for the course `Numerical Methods for Partial Differential Equations` (401-0674-00L)

Tested:
- Ubuntu 14.04-2
- Mint 17.1 Rebecca
- Fedora 21
- CentOS 6.6 (brutus cluster works out of the box without any further preperation)
- Mac OS X Yosemite (Build 14C81h, 14C109)

__Important note__ There were several bug fixes in the 0.4 beta release. See the Updates section on how to update.

## Prepare development machine

In order to setup the development enviroment you need certain packages to be installed before proceeding. Just install them with the package manager of your yoice (apt-get, yum tested).

Feel free to test the environment on your own distribution. You can submit the results or ask for support at https://gitlab.phys.ethz.ch/tille/npde15-env/issues

### Linux

__Debian__ (e.g. Ubuntu, Linux Mint)
Recommended packages (may be installed by apt-get; Note: these list is not rigorously tested yet)

```
# Build environment
build-essential
gfortran

# linear algebra / math libraries
libblas-dev
liblapack-dev
libquadmath0
libgfortran3
libeigen3-dev

libtool
autoconf
automake

# VCS
git

# x11 header files
libx11-dev

# opengl bindings
libglapi-mesa # required for libgl1-mesa-dev
libgl1-mesa-dev
```

```
	sudo apt-get install build-essential gfortran git libblas-dev liblapack-dev libquadmath0 libgfortran3 libeigen3-dev libtool autoconf automake libx11-dev libglapi-mesa libgl1-mesa-dev
```

__RedHat__ (e.g. Fedora, Cent OS; experimentally)

```
	sudo yum install patch gcc gcc-c++ git gcc-gfortran libX11-devel blas-devel eigen3-devel lapack lapack-devel mesa-libGL-devel automake autoconfy libtool
```

### Mac

- Install Xcode from the AppStore (make sure to accept the license agreement)
	*Note* If you already have Xcode installed  make sure its the most recent version!
- Install homebrew

	```
	ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	brew update
	```
	*Note*: if you have already a bunch of software on your machine (e.g. macports) make sure brew is setup correctly by `brew doctor`
- Install required packages
	- Install xQuartz from http://xquartz.macosforge.org

	*Note* gfortran compiler is provided in gcc formula

	```
	brew install wget git gcc automake libtool pkg-config eigen

	brew tap homebrew/science
	brew install homebrew/science/openblas
	```

	*Note* if you face errors caused by missing packages (while compiling), you can rebuild the library that failed by removing the corresponding directory in `opt/package_name` and `build/package_name` and rerun the bootstrap script

*Note* Since the pardiso solver used in the lecture example depends on openMP there will most likely be no release in the near future that uses clang (unless openMp support is added to clang trunk). If you still want to use clang you need to compile it yourself with openMP support (see http://clang-omp.github.io/#try-openmp-clang).

## Bootstrap env

To start working with your environment just checkout the git repo and run the shell script

	git clone https://gitlab.phys.ethz.ch/tille/npde15-env.git
	cd npde15-env
	./bootstrap.sh

If the installation finished successful you should see something like

	Libraries have been installed to /the/path/your/working/in/opt

Please don't move the folder or you won't be able to run the lecture example (hopefully there will be a fix soon)!

### Updates

Run the following command in the `npde15-env` directory

	git pull
	./clean.sh
	./bootstrap.sh

### Pardiso solver
The lecture example makes usage of the `pardiso` solver. You can apply for a license at http://www.pardiso-project.org/. You will recieve an email that is required to run the `bootstrap` script completly!

*Note for Mac OS X*: Make sure you use the right version of `libtool` (e.g. GNU glibtool or the one included in the xcode toolchain) for the lecture example (`/Library/Developer/CommandLineTools/usr/bin/libtool` does __not__ work)

Directory structure

	- bootstrap.sh
	- clean.sh
	- files/
	- patch/
	[-build/
		alberta-2.0.1
		ALUGrid-1.52
		metis-4.0
	]
	[-opt/
		alberta/
		alugrid/
		dune/
			dune-common/
			dune-*/ ...
			parallel-debug.opts
			parallel.opts
		metis/
		pardiso/
	]

## Test your environment

To run the lecture example you need to make some changes to the makefile. There is a patch supplied in `patch/lecture_example/makefile.path` that does the job automatically. Just change to the directory of the lecture example (the directory where the makefile is located)

	patch < ${NPDE_BUILD_ENV_PATH}/../patch/lecture_example/makefile.patch

*Note* Apply the patch only once (or you will be asked whether you want to reverse it)!

*Note* You can __not__ just download the lecture example and start over. Make sure that the `npde15 folder is at `../../../` relative from the makefile (there will be a better documentation of this soon).

## Third party applications

![Screenshot](screenshot.png "Screenshot")

<br>

## Data analysis

You can use Paraview (http://www.paraview.org/) to open `.vtu` files.

## Mesh viewer

You can use Gmseh (http://geuz.org/gmsh/) to open `.msh` files.

# Contact

Till Ehrengruber
tille[at]student.ethz.ch

NPDE15 course presence hours
Thursday from 17:15 to 18:00 in HG G 26.3 (attention: this information is not updated frequently)

# Troubleshooting

If you face problems, first run `./clean.sh` and then retry `./bootstrap.sh`. If this didn't fix the problem submit a Bug Report.

- pardiso library complains about missing .dylib files
	This is a bug that was fixed in the 0.4 beta release. Try to checkout the newest version.

# Issues, Bug Reports

In order to submit Issues, Bug Reports, Merge Request you need a D-Phys account or write me an email.

# TODO

- make bootstrap script a homebrew formula
- currently all libraries are installed in the root of the repos directory structure by default
- update dune library
  	in autumn 2014 the dune library has switched from automake to cmake
- use clang on Mac OS X (adapt CXXFLAGS)
- Terminal colors can very on different platforms (use black background)
- use homebrew formula for lapack on Mac OS X
- maybe it is more appropiate to use the intel mkl paradiso solver (see https://software.intel.com/en-us/node/470282) for licensing and usability reasons
- make the environment portable (essentially the problem is that libtool is used since .la files are non portable)