#!/bin/bash
METIDS_PATH=../../metis-4.0

# patch files with sed
sed "s/int\ log2(int)/int\ Log2(int)/" $METIDS_PATH/Lib/proto.h > proto.h.patched
sed "s/int\ log2(int\ a)/int\ Log2(int\ a)/" $METIDS_PATH/Lib/util.c > util.c.patched
for file in kmetis.c kvmetis.c mkmetis.c; do
  sed "s/log2(/Log2(/g" $METIDS_PATH/Lib/$file > ${file}.patched
done

# create patches
diff -u $METIDS_PATH/Lib/proto.h proto.h.patched > proto.h.patch
diff -u $METIDS_PATH/Lib/util.c util.c.patched > util.c.patch
diff -u $METIDS_PATH/Lib/kmetis.c kmetis.c.patched > kmetis.c.patch
diff -u $METIDS_PATH/Lib/kvmetis.c kvmetis.c.patched > kvmetis.c.patch
diff -u $METIDS_PATH/Lib/mkmetis.c mkmetis.c.patched > mkmetis.c.patch

# cleanup
rm *.patched