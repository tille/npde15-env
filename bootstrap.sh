#!/bin/bash
#set -x

BASE_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
DOWNLOAD_PATH="$BASE_DIR/files"
BUILD_PATH="$BASE_DIR/build"
INSTALL_PATH="$BASE_DIR/opt" # todo configureable
EXIT_ON_ERROR=true

pushd $BASE_DIR > /dev/null

# color helpers
function heading () {
	printf '\e[48;1;4m%s\e[0m \n' "$1"
}

function notice () {
	printf '\e[0;32m%s\e[0m \n' "$1"
}

function error () {
	printf '\e[41m%s\e[0m \n' "$1"
	exit;
}

function warn () {
	printf '\e[48;5;208m%s\e[0m \n' "$1"
}

function color_grad () {
	if [ "$1" -lt "$2" ]; then
		for (( i=$1; i<=$2; i++ )); do printf "\e[48;5;${i}m \e[0m" ; done ;
	else
		for (( i=$1; i>=$2; i-- )); do printf "\e[48;5;${i}m \e[0m" ; done ;
	fi
}

function fancy_heading () {
	#color_grad 16 21
	printf "%-60s" "$1"
	#color_grad 21 16
	printf "\n"
}

function fancy_sep () {
	local colored_ws=$(printf "%60s" " " | sed -e 's/./\\e[48;5;21m \\e[0m/g');

	#color_grad 16 21
	#printf "$colored_ws"
	#color_grad 21 16
	printf "\n"
}

function colored_ws () {
	#local colored_ws=$(echo "$2" | sed -e 's/./\\e[48;5;'"$1"'m \\e[0m/g');
	local colored_ws=$(printf "%$2s" " " | sed -e 's/./\\e[48;5;'"$1"'m \\e[0m/g');
	#printf "$colored_ws";
}

# error handler
function error_handler () {
	local signal=$?
	if $EXIT_ON_ERROR; then
		# exit on error with error message
		[ "$signal" -eq 0 ] || error "uncaught exception occured"
	else
		[ "$signal" -eq 0 ] || warn "uncaught exception occured"
	fi
}

trap error_handler ERR # use 'trap - ERR' to remove

# heading
fancy_sep;
fancy_heading "      ____________________   ___"
fancy_heading "     /  ________   ___   /__/  /"
fancy_heading "    /  _____/  /  /  /  ___   /"
fancy_heading "   /_______/  /__/  /__/  /__/"
fancy_heading "   Eidgenoessische Technische Hochschule Zuerich"
fancy_heading "   Swiss Federal Institute of Technology Zurich"
fancy_heading "------------------------------------------------------------"
fancy_heading "    Numerical Methods for Partial Differential Equations    "
fancy_heading "    (Spring 2015, 401-0674-00L)"
fancy_heading "------------------------------------------------------------"
fancy_heading " Build environment (beta 0.4)"
fancy_heading " following libraries will be installed:"
fancy_heading " - metis 4.0.1"
fancy_heading " - alberta 2.0.1"
fancy_heading " - ALUGrid 1.5.2"
fancy_heading " - pardiso 5.0.0"
fancy_heading " - dune (custom patched)"
fancy_heading " - [eigen master (only if not preinstalled)]"
fancy_heading "------------------------------------------------------------"
fancy_heading " Contact"
fancy_heading "   Till Ehrengruber"
fancy_heading "   tille@student.ethz.ch"
fancy_heading " Documentation"
fancy_heading "   https://gitlab.phys.ethz.ch/tille/npde15-env"
fancy_sep;

# array join helper
function join { local IFS="$1"; shift; echo "$*"; }

# detect platform
platform='unknown'
unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
   platform='linux'
elif [[ "$unamestr" == 'Darwin' ]]; then
   platform='darwin'
elif [[ "$unamestr" == 'FreeBSD' ]]; then
   platform='freebsd'
fi

#
# Download libraries
#
if [ ! -d files ]; then
	mkdir files
fi
pushd $DOWNLOAD_PATH > /dev/null

# Download dune library
dune_modules=( 
	common
	geometry
	grid
	istl
	localfunctions
	#grid-howto
	#grid-dev-howto
)

heading "Download dune modules"

#for module in "${dune_modules[@]}"
#do
#	module="dune-$module"
#	if [ -d $module ] || [ -L $module ]; then # check whether dir already exists (or is a symlink)
#		notice "Skipping $module"
#	else
#		git clone http://git.dune-project.org/repositories/$module
#	fi
#done

# workaround until further details about the used version of dune are known
if [ ! -d dune-common ]; then
	tar xzf dune_npde.tar.gz
	mv dune/dune-* .
	rm -r dune
else
	notice "Skipped"
fi

# download metis
#  see http://glaros.dtc.umn.edu/gkhome/fsroot/sw/metis/OLD
heading "Download metis (4.0.1)"
if [ -f "metis-4.0.1.tar.gz" ]; then
	notice "Skipping"
else
	wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/OLD/metis-4.0.1.tar.gz
fi

# download alberta
#  see http://www.alberta-fem.de/download.html
#  see http://www.mathematik.uni-stuttgart.de/fak8/ians/lehrstuhl/nmh/downloads/alberta/
#  todo: use svn & check certificate (see http://serverfault.com/questions/27006/svn-wont-accept-my-invalid-certificate)
heading "Download alberta (2.0.1)"
#svn checkout --username=alberta --password=alberta https://www.ians.uni-stuttgart.de/subversion/alberta/trunk/albertadist
if [ -f "alberta-2.0.1.tar.gz" ]; then
	notice "Skipping"
else
	wget http://www.alberta-fem.de/Downloads/alberta-2.0.1.tar.gz
fi

# download alugrid
#	TODO: this package is depreceated update to 2.3 and integrate it as a git submodule
heading "Download ALUGrid (1.52)"
if [ -f "ALUGrid-1.52.tar.gz" ]; then
	notice "Skipping"
else
	wget http://dune.mathematik.uni-freiburg.de/downloads/ALUGrid-1.52.tar.gz
fi

popd > /dev/null # leave $DOWNLOAD_PATH

#
# build libraries
#
# create build directory
if [ ! -d "$BUILD_PATH" ]; then
	mkdir $BUILD_PATH
fi
# create installation directory
if [ ! -d "$INSTALL_PATH" ]; then
	mkdir $INSTALL_PATH
fi

pushd $BUILD_PATH > /dev/null

# specify compiler
if [ "$platform" == "darwin" ]; then
	# TODO: move to clang
	F77=gfortran
	CC=gcc-4.9
	MPICC=mpicc
	CXX=g++-4.9
	# darwin - clang (not working yet)
	#CC=cc
	#CXX=g++
else
	F77=gfortran
	CC=gcc
	MPICC=mpicc
	CXX=g++
fi

# specify compiler flags
if [ ! "$CXXFLAGS" ]; then
	CXXFLAGS="-O3 -DNDEBUG"
	# clang use -Wno-reserved-user-defined-literal to supress some compiler errors
fi
CFLAGS="$CXXFLAGS"

# download precompiled pardiso solver
heading "Download pardiso"
notice "you can apply for an evaluation license at http://www.pardiso-project.org/branch"
if [ ! -d $INSTALL_PATH/pardiso ] || [ ! -f $INSTALL_PATH/pardiso/pardiso.lic ]; then
	notice ""
	notice "please enter the download url you retrieved in the email: (e.g. http://www.pardiso-project.org/download/xxxxxxxx)"
	read pardiso_download_url
	#$INSTALL_PATH

	pardiso_regex="http://www.pardiso-project.org/download/[^/]+";
	if [[ ! $pardiso_download_url =~ $pardiso_regex ]]; then
		notice "Url did not match. Did you misspell the url?"
		notice "Proceed anyway? [yes, no]:"
		read pardiso_proceed
		if [ $pardiso_proceed != "yes" ]; then
			error "Exiting since pardiso could not be downloaded"
		fi
		notice "Skipped (by user input)"
	else
		if [ ! -d $INSTALL_PATH/pardiso ]; then
			mkdir "$INSTALL_PATH/pardiso"
		fi

		# download library
		if [ "$platform" == "darwin" ]; then
			wget --directory-prefix="$INSTALL_PATH/pardiso/" "${pardiso_download_url}/libpardiso500-MACOS-X86-64.dylib"
		else
			wget --directory-prefix="$INSTALL_PATH/pardiso/" "${pardiso_download_url}/libpardiso500-GNU481-X86-64.so"
		fi

		# set environment variable
		if [ ! $SHELL == "/bin/bash" ]; then
			notice "${SHELL} is not supported yet. Define PARDISO_LIC_PATH environmental variable manuelly!"
		fi

		if [ ! -f ~/.bashrc ]; then
			touch ~/.bashrc
			chmod 700 ~/.bashrc
		fi

		trap - Err
		$(cat ~/.bashrc 2>/dev/null | grep PARDISO_LIC_PATH)
		PARDISO_LIC_FOUND=$?

		if [ $PARDISO_LIC_FOUND -eq 0 ]; then
			PARDISO_LIC_PATH=$(awk -F'"' '/PARDISO_LIC_PATH=/ {print $2}' ~/.bashrc)

			if [ ! -d "${PARDISO_LIC_PATH}" ]; then
				warn "Pardiso license path in ~/.bashrc was invalid. Trying to fix this."
				sed -i.bak 's/export PARDISO_LIC_PATH="[a-zA-Z0-9]*"//' ~/.bashrc
				PARDISO_LIC_FOUND=1
			fi
		fi

		if [ ! $PARDISO_LIC_FOUND -eq 0 ]; then
			notice "Please enter the license key you recieved: (without whitespaces)"
			read pardiso_license
			echo "${pardiso_license}" > "$INSTALL_PATH/pardiso/pardiso.lic"
			echo "export PARDISO_LIC_PATH=\"$INSTALL_PATH/pardiso\"" >> ~/.bashrc
		else
			notice "Pardiso license was already found"
		fi

		trap error_handler Err
	fi
else
	notice "Skipping"
fi

#
# eigen
#
heading "Download/link Eigen"
if ( [ -d "$INSTALL_PATH/eigen" ] || [ -L "$INSTALL_PATH/eigen" ] ) && [ ! "$rebuild" ]; then
	notice "Skipping"
else
	if [ "$platform" == "darwin" ]; then
		(brew list | grep eigen) > /dev/null
		if [ $? -eq 1 ]; then
			error "Eigen not installed (install via \`brew install eigen\`)"
		fi
		EIGEN_PATH=$(brew --prefix eigen)"/include/eigen3"
	elif [ -d "/usr/include/eigen3" ]; then # try /usr/include/eigen3 - default path in ubuntu 14.04
		EIGEN_PATH="/usr/include/eigen3"
	fi

	if [ $EIGEN_PATH ]; then
		notice "creating symlink from ${EIGEN_PATH} to ${INSTALL_PATH}/eigen"
		ln -s "${EIGEN_PATH}" "$INSTALL_PATH/eigen"
	else
		notice "download eigen from https://github.com/RLovelett/eigen"
		pushd "$INSTALL_PATH" > /dev/null
		git clone https://github.com/RLovelett/eigen
		popd > /dev/null
	fi
fi

#
# metis
#
heading "Build metis (4.0.1)"
if ( [ -d "$INSTALL_PATH/metis" ] || [ -L "$INSTALL_PATH/metis" ] ) && [ ! "$rebuild" ]; then
	notice "Skipping"
else
	if [ ! -d "metis-4.0" ]; then
		notice "Extract"
		tar xzf "$DOWNLOAD_PATH/metis-4.0.1.tar.gz"
	fi

	notice "Cleanup"
	if [ -d "$INSTALL_PATH/metis" ]; then
		rm -r $INSTALL_PATH/metis
	fi 

	# unroll patches
	#pushd metis-4.0/Lib > /dev/null
	#notice "Unroll patches"
	#patch --batch -R < $BASE_DIR/patch/metis/proto.h.patch
	#patch --batch -R < $BASE_DIR/patch/metis/util.c.patch
	#patch --batch -R < $BASE_DIR/patch/metis/kmetis.c.patch
	#patch --batch -R < $BASE_DIR/patch/metis/kvmetis.c.patch
	#patch --batch -R < $BASE_DIR/patch/metis/mkmetis.c.patch
	#trap - ERR
	#popd > /dev/null

	pushd metis-4.0 > /dev/null
	notice "Patch some files"
	pushd Lib > /dev/null
	patch < $BASE_DIR/patch/metis/proto.h.patch
	patch < $BASE_DIR/patch/metis/util.c.patch
	patch < $BASE_DIR/patch/metis/kmetis.c.patch
	patch < $BASE_DIR/patch/metis/kvmetis.c.patch
	patch < $BASE_DIR/patch/metis/mkmetis.c.patch
	popd > /dev/null # leave metis-4.0/lib

	# Make
	notice "make"
	make $MAKE_FLAGS

	if [ ! $? -eq 0 ]; then
		error "Build failed"
	fi
	popd > /dev/null # leave metis-4.0

	notice "Copy metis build directory to installation directory"
	mv "metis-4.0" "$INSTALL_PATH/metis"

	notice "Finished building metis"
fi

#
# alberta
#
heading "Build alberta (2.0.1)"
if ([ -d "$INSTALL_PATH/alberta" ] || [ -L "$INSTALL_PATH/alberta" ]) && [ ! "$rebuild" ]; then
	notice "Skipping"
else
	if [ ! -d "alberta-2.0.1" ]; then
		notice "Extract"
		tar xzf "$DOWNLOAD_PATH/alberta-2.0.1.tar.gz"
	fi

	pushd alberta-2.0.1 > /dev/null
		ALBERTA_FLAGS=(
			--without-x
			--enable-shared=no
			--with-blas-name=blas
			--prefix="$INSTALL_PATH/alberta"
		)
		ALBERTA_LDFLAGS="$LDFLAGS"
		ALBERTA_CPPFLAGS="$CPPFLAGS"
		ALBERTA_CXXFLAGS="$CXXFLAGS"
		ALBERTA_CFLAGS="$CFLAGS"

		if [ "$platform" == "darwin" ]; then
			ALBERTA_FLAGS+=(--with-opengl-name=OpenGL)
			ALBERTA_LDFLAGS+=" -L /opt/X11/lib -L/System/Library/Frameworks/OpenGL.framework"
			ALBERTA_CPPFLAGS+=" -I/opt/X11/include -I/usr/local/opt/openblas/include" # c & c++
		fi

		# Configure
		notice "configure"
		(./configure \
			CC=$CC \
			CXX=$CXX \
			F77=$F77 \
			CPPFLAGS="$ALBERTA_CPPFLAGS" \
			CXXFLAGS="$ALBERTA_CXXFLAGS" \
			CFLAGS="$ALBERTA_CFLAGS" \
			LDFLAGS="$ALBERTA_LDFLAGS" \
			${ALBERTA_FLAGS[@]}
		)

		if [ ! $? -eq 0 ]; then
			error "Configure failed"
		fi

		# Make
		notice "make"
		make $MAKE_FLAGS clean install

		if [ ! $? -eq 0 ]; then
			error "Build failed"
		fi
	popd > /dev/null
	notice "Finished building alberta"
fi

#
# ALUGrid
#
heading "Build ALUGrid (1.52)"
if ([ -d "$INSTALL_PATH/alugrid" ] || [ -L "$INSTALL_PATH/alugrid" ]) && [ ! "$rebuild" ]; then
	notice "Skipping"
else
	if [ ! -d "ALUGrid-1.52" ]; then
		notice "Extract"
		tar xzf "$DOWNLOAD_PATH/ALUGrid-1.52.tar.gz"
	fi

	pushd ALUGrid-1.52 > /dev/null
	ALUGRID_FLAGS=(
		--prefix="$INSTALL_PATH/alugrid" \
		--with-metis="$INSTALL_PATH/metis"
	)
	ALUGRID_LDFLAGS="$LDFLAGS `$DOWNLOAD_PATH/dune-common/bin/mpi-config --libs --disable-cxx --mpicc=$MPICC`"
	ALUGRID_CPPFLAGS="-DNDEBUG $CPPFLAGS `$DOWNLOAD_PATH/dune-common*/bin/mpi-config --cflags --disable-cxx --mpicc=$MPICC`" # c & c++
	ALUGRID_CXXFLAGS="$CXXFLAGS"
	ALUGRID_CFLAGS="$CFLAGS"

	# Configure
	notice "configure"
	(./configure \
		CC=$CC \
		CXX=$CXX \
		F77=$F77 \
		CPPFLAGS="$ALUGRID_CPPFLAGS" \
		CXXFLAGS="$ALUGRID_CXXFLAGS" \
		CFLAGS="$ALUGRID_CFLAGS" \
		LDFLAGS="$ALUGRID_LDFLAGS" \
		${ALUGRID_FLAGS[@]}
	)
	# Make
	notice "make"
	make $MAKE_FLAGS clean install

	popd > /dev/null # leave ALUGrid-1.52
	notice "Finished building ALUGrid"
fi

#
# Dune
#
heading "Build dune"
if ([ -d "$INSTALL_PATH/dune" ] || [ -L "$INSTALL_PATH/dune" ]) && [ ! "$rebuild" ]; then
	notice "Skipping"
else
	# copy dune files to installation path
	notice "Copy files"
	mkdir $INSTALL_PATH/dune
	cp -r "$DOWNLOAD_PATH/dune-"* $INSTALL_PATH/dune

	pushd $INSTALL_PATH/dune > /dev/null
	DUNE_FLAGS=(
		--prefix=\"$INSTALL_PATH/dune\"
		--disable-documentation
		--disable-mpiruntest
		--enable-parallel
		--with-alugrid=\"$INSTALL_PATH/alugrid\"
		--with-alberta=\"$INSTALL_PATH/alberta\"
	)
	DUNE_CXXFLAGS=(
		-O3
		-march=native
		-g0
		-funroll-loops
		-ftree-vectorize
		-fno-strict-aliasing
		-Wall
		-Wno-deprecated-declarations
	)
	DUNE_CFLAGS=(
		-O3
		-march=native
		-g0
		-funroll-loops
		-ftree-vectorize
		-fno-strict-aliasing
		-Wall
		-Wno-deprecated-declarations
	)
	DUNE_DEBUG_FLAGS=("${DUNE_FLAGS[@]}")
	DUNE_DEBUG_CXXFLAGS=(${DUNE_CXXFLAGS[@]})
	DUNE_DEBUG_CFLAGS=(${DUNE_CFLAGS[@]})

	if [ "$platform" == "darwin" ]; then
		#DUNE_CXXFLAGS+=(-mno-avx)
		#DUNE_CFLAGS+=(-mno-avx) # todo document
		DUNE_CXXFLAGS+=(-Xassembler -q) # use clang assembler, todo: use -Wa,-q
		DUNE_CFLAGS+=(-Xassembler -q)
	fi

	# write parallel-debug.opts
	echo -e CONFIGURE_FLAGS=\" > parallel-debug.opts
		echo -e $(printf '%q ' ${DUNE_DEBUG_FLAGS[@]})" \c" >> parallel-debug.opts
		echo -e CXX=$CXX" \c" >> parallel-debug.opts
		echo -e CC=$CC" \c" >> parallel-debug.opts
		echo -e CXXFLAGS=\\\"$(printf '%q ' ${DUNE_DEBUG_CXXFLAGS[@]})\\\"" \c" >> parallel-debug.opts
		echo -e CFLAGS=\\\"$(printf '%q ' ${DUNE_DEBUG_CFLAGS[@]})\\\"" \c" >> parallel-debug.opts
	echo -e \" >> parallel-debug.opts

	# write parallel.opts
	echo -e CONFIGURE_FLAGS=\" > parallel.opts
		echo -e $(printf '%q ' ${DUNE_FLAGS[@]})" \c" >> parallel.opts
		echo -e CXX=$CXX" \c" >> parallel.opts
		echo -e CC=$CC" \c" >> parallel.opts
		echo -e CXXFLAGS=\\\"$(printf '%q ' ${DUNE_CXXFLAGS[@]})\\\"" \c" >> parallel.opts
		echo -e CFLAGS=\\\"$(printf '%q ' ${DUNE_CFLAGS[@]})\\\"" \c" >> parallel.opts
	echo -e \" >> parallel.opts

	# build
	notice "build with dunecontrol"
	dune-common/bin/dunecontrol --opts=parallel.opts all

	popd > /dev/null # leave $INSTALL_PATH/dune

	# move to installation path
	notice "Finished building dune"
fi

#
# fix pardiso solver dylib paths
#
# this is neccessary to avoid using DYLD_LIBRARY_PATH
# fetch library search path from dune-common config.status
#  todo: remove dependency on dune-common config.status
if [ "$platform" == "darwin" ]; then
	LIB_SEARCH_PATHS=$(awk -F"\'" '/^sys_lib_search_path_spec=/ {print $2}' "$INSTALL_PATH/dune/dune-common/config.status")
	eval "LIB_SEARCH_PATHS=( $LIB_SEARCH_PATHS )"

	PARDISO_LIB_PATHS=$(otool -L "$INSTALL_PATH/pardiso/libpardiso500-MACOS-X86-64.dylib")
	PARDISO_LIB_PATHS=$(echo "${PARDISO_LIB_PATHS}" | sed 1d | sed 1d) # ignore first and second line
	PARDISO_LIB_PATHS=$(echo "${PARDISO_LIB_PATHS}" | sed 's/[  ]*\(.*\)(.*)/\1/') # remove version information
	PARDISO_LIB_PATHS=$(echo "${PARDISO_LIB_PATHS}" | sed -E 's/^[[:space:]]+//'| grep -v '^$') # trim
	PARDISO_LIB_PATHS=$(echo "${PARDISO_LIB_PATHS}" | tr -s '\n' ' ') # replace newline char with whitespace
	eval "PARDISO_LIB_PATHS=( $PARDISO_LIB_PATHS )"

	for PARDISO_LIB_PATH in "${PARDISO_LIB_PATHS[@]}"
	do
		if [ "${PARDISO_LIB_PATH##*.}" == "dylib" ] && [ ! -f "${PARDISO_LIB_PATH}" ]; then
			notice "${PARDISO_LIB_PATH} was not found. trying to fix this"

			for LIB_SEARCH_PATH in "${LIB_SEARCH_PATHS[@]}"
			do
				LIB_SEARCH_FILEPATH="$LIB_SEARCH_PATH/"$(basename "${PARDISO_LIB_PATH}")
				if [ -f "$LIB_SEARCH_FILEPATH" ]; then
					echo "found at $LIB_SEARCH_FILEPATH"
					install_name_tool -change "${PARDISO_LIB_PATH}" "${LIB_SEARCH_FILEPATH}" "$INSTALL_PATH/pardiso/libpardiso500-MACOS-X86-64.dylib"
					break;
				fi
			done
		fi
	done

	# fix absolute path of the library
	install_name_tool -id "$INSTALL_PATH/pardiso/libpardiso500-MACOS-X86-64.dylib" "$INSTALL_PATH/pardiso/libpardiso500-MACOS-X86-64.dylib"
fi

# set NPDE_BUILD_ENV_PATH env variable
NPDE_BUILD_ENV_PATH=$(awk -F'"' '/NPDE_BUILD_ENV_PATH=/ {print $2}' ~/.bashrc)
if [ ! -d "$NPDE_BUILD_ENV_PATH" ]; then
	# remove old NPDE_BUILD_ENV_PATH
	sed -i.bak 's/export NPDE_BUILD_ENV_PATH="[^"]*"//' ~/.bashrc

	echo "export NPDE_BUILD_ENV_PATH=\"${INSTALL_PATH}\"" >> ~/.bashrc
fi

popd > /dev/null # leave $INSTALL_PATH
popd > /dev/null # leave $BASE_DIR

#
# display debug information
#
heading "Installation completed"
notice "to complete the installation run \`source '~/.bashrc'\` or close the current terminal window"
notice "Libraries have been installed to ${INSTALL_PATH}"
